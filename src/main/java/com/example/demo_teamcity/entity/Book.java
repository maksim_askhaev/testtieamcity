package com.example.demo_teamcity.entity;

import javax.persistence.Entity;

@Entity
public class Book extends BaseIdEntity {

    private String name;

    public Book(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
